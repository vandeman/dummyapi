# README #

Dummy Web API is a simple service to quickly create a web service that returns data. You can specify the data you want returned and the service will create a unique URI for you to call in your application or even unit (or semi-integration) tests.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Current features ###
Parse and categorise response body on the fly (JSON, XML, plain text)

### Future features ###
Request history
Respond with files
Return custom headers
Placeholders for dynamic responses
Request validation
SOAP Support - WSDL Reading/Writing, XSLT etc.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Add bugs and feature requests (enhancements) to the issue tracker